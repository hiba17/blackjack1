import VueRouter from 'vue-router'
import Play from './components/Play.vue'
import StartScreen from './components/StartScreen.vue'
import Questions from './components/Questions.vue'

const routes = [
    {
        path: '/',
        component: StartScreen
    },
    {
        path: '/questions',
        component: Questions
    },
    {
        path: '/play/:playerName',
        name: 'Play',
        component: Play,
        props: true
    }
]

const router = new VueRouter({ routes })

export default router

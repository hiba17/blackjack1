export function fetchDeck() {
    return fetch("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
        .then(response => response.json())
}

export function drawCards(deck_id) {
    console.log(deck_id)
    return fetch("https://deckofcardsapi.com/api/deck/"+deck_id+"/draw/?count=2")
        .then(response => response.json())
}

export function drawOneCard(deck_id) {
    console.log(deck_id)
    return fetch("https://deckofcardsapi.com/api/deck/"+deck_id+"/draw/?count=1")
        .then(response => response.json())
}

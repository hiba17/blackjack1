# blackjack

## Collaborators
Hiba Benhaida and Thomas Gulli

## Problems
Had some problems with git merge and project set up at the start, so used too much time on that.
Had a lot of bugs that took much time to solve...

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
